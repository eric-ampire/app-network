<?php

/**
 * Network interface controller.
 *
 * @category   apps
 * @package    network
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2011-2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/network/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\network\Iface as IfaceAPI;
use \clearos\apps\network\Role as Role;

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Network interface controller.
 *
 * @category   apps
 * @package    network
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2011-2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/network/
 */

class Iface extends ClearOS_Controller
{
    /**
     * Network interface summary.
     *
     * @return view
     */

    function index()
    {
        // Load libraries
        //---------------

        $this->load->library('network/Iface_Manager');
        $this->load->library('network/Network_Status');

        // Load view data
        //---------------

        try {
            $iface_options['filter_virtual'] = FALSE;
            $iface_options['filter_vlan'] = FALSE;
            $iface_options['filter_minions'] = FALSE;

            $data['form_type'] = ($this->session->userdata('wizard')) ? 'wizard' : 'view';
            $data['network_status'] = $this->network_status->get_connection_status();
            $data['network_interfaces'] = $this->iface_manager->get_interface_details($iface_options);
            $data['external_interfaces'] = $this->iface_manager->get_external_interfaces();
            $data['interface_types'] = $this->iface_manager->get_interface_types();

            $data['available_interface_count'] = 0;

            foreach ($data['network_interfaces'] as $iface => $details) {
                if ($details['configurable'] && !$details['configured'])
                    $data['available_interface_count']++;
            }
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load views
        //-----------

        $page_options['type'] = (clearos_console()) ? MY_Page::TYPE_CONSOLE : NULL;

        $this->page->view_form('network/ifaces', $data, lang('network_interfaces'), $page_options);
    }

    /**
     * Add interface view.
     *
     * @param string $interface interface
     *
     * @return view
     */

    function add($interface = NULL)
    {
        $this->_item('add', $interface);
    }

    /**
     * Add bridged interface view.
     *
     * @return view
     */

    function add_bridge()
    {
        // Load libraries
        //---------------

        $this->load->library('network/Iface_Manager');

        // Set validation rules
        //---------------------

        $this->form_validation->set_policy('bridge', 'network/Iface', 'validate_interface', TRUE);

        $form_ok = $this->form_validation->run();

        // Handle form submit
        //-------------------

        if ($this->input->post('submit') && ($form_ok === TRUE)) {
            try {
                $slaves = [];
                $unconfigured = $this->iface_manager->get_unconfigured_interfaces();

                foreach ($unconfigured as $slave) {
                    $enabled = $this->input->post('slave_' . $slave);

                    if ($enabled)
                        $slaves[] = $slave;
                }

                $this->iface_manager->save_bridge($this->input->post('bridge'), $slaves);
                $this->page->set_message(lang('network_bridge_created'), 'info');
                redirect('/network/iface/edit/' . $this->input->post('bridge'));
            } catch (Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }

        // Load the view data
        //-------------------

        try {
            $data['bridge'] = $this->iface_manager->get_next_bridge_name();
            $data['unconfigured'] = $this->iface_manager->get_unconfigured_interfaces();
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load the views
        //---------------

        $page_options['type'] = (clearos_console()) ? MY_Page::TYPE_CONSOLE : NULL;

        $this->page->view_form('network/add_bridge', $data, lang('network_add_bridge'), $page_options);
    }

    /**
     * Add virutal interface view.
     *
     * @return view
     */

    function add_virtual()
    {
        $this->_virtual_item('add');
    }

    /**
     * Add VLAN interface view.
     *
     * @return view
     */

    function add_vlan()
    {
        $interface = $this->input->post('iface');

        $this->_vlan_item('add', $interface);
    }

    /**
     * Delete interface view.
     *
     * @param string $interface interface
     *
     * @return view
     */

    function delete($interface = NULL)
    {
        $confirm_uri = '/app/network/iface/destroy/' . $interface;
        $cancel_uri = '/app/network/iface';
        $items = array($interface);

        $page_options['type'] = (clearos_console()) ? MY_Page::TYPE_CONSOLE : NULL;

        $this->page->view_confirm_delete($confirm_uri, $cancel_uri, $items, $page_options);
    }

    /**
     * Edit interface view.
     *
     * @param string $interface interface
     *
     * @return view
     */

    function edit($interface = NULL)
    {
        $this->_item('edit', $interface);
    }

    /**
     * Edit virtual interface view.
     *
     * @param string $interface interface
     *
     * @return view
     */

    function edit_virtual($interface = NULL)
    {
        $this->_virtual_item('edit', $interface);
    }

    /**
     * Edit VLAN interface view.
     *
     * @param string $interface interface
     *
     * @return view
     */

    function edit_vlan($interface = NULL)
    {
        $this->_vlan_item('edit', $interface);
    }

    /**
     * Destroys interface.
     *
     * @param string $interface interface
     *
     * @return view
     */

    function destroy($interface = NULL)
    {
        // Load libraries
        //---------------

        $this->load->library('network/Iface', $interface);
        $this->load->library('network/Role');
        $this->load->library('network/Routes');

        // Handle delete
        //--------------

        try {
            $this->iface->delete_config();
            $this->role->remove_interface_role($interface);

            $current_route = $this->routes->get_gateway_device();

            if ($role === Role::ROLE_EXTERNAL) {
                $this->routes->set_gateway_device($interface);
            } else if ($interface == $current_route) {
                $this->routes->delete_gateway_device();
            }

            $this->page->set_status_deleted();
            redirect('/network/iface');
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }
    }

    /**
     * View interface view.
     *
     * @param string $interface interface
     *
     * @return view
     */

    function view($interface = NULL)
    {
        $this->_item('view', $interface);
    }

    ///////////////////////////////////////////////////////////////////////////////
    // P R I V A T E
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Common add/edit/view form handler.
     *
     * @param string $form_type form type
     * @param string $interface interface
     *
     * @return view
     */

    function _item($form_type, $interface)
    {
        // Load libraries
        //---------------

        $this->lang->load('network');
        $this->load->library('network/Iface', $interface);
        $this->load->library('network/Iface_Manager');
        $this->load->library('network/Role');
        $this->load->library('network/Proxy');

        // TODO: remove deprecated dhcp/Dnsmasq in ClearOS 8
        if (clearos_library_installed('dhcp/Subnets_Class'))
            $this->load->library('dhcp/Subnets_Class');
        else if (clearos_library_installed('dhcp/Dnsmasq'))
            $this->load->library('dhcp/Dnsmasq');

        // Set validation rules
        //---------------------

        $bootproto = $this->input->post('bootproto');
        $role = $this->input->post('role');

        $this->form_validation->set_policy('role', 'network/Role', 'validate_role', TRUE);
        $this->form_validation->set_policy('bootproto', 'network/Iface', 'validate_boot_protocol', TRUE);

        if ($bootproto == IfaceAPI::BOOTPROTO_STATIC) {
            $this->form_validation->set_policy('ipaddr', 'network/Iface', 'validate_ip', TRUE);
            $this->form_validation->set_policy('netmask', 'network/Iface', 'validate_netmask', TRUE);

            // TODO: remove deprecated dhcp/Dnsmasq in ClearOS 8
            if ($role == Role::ROLE_EXTERNAL)
                $this->form_validation->set_policy('gateway', 'network/Iface', 'validate_gateway', TRUE);
            else if (clearos_library_installed('dhcp/Subnets_Class'))
                $this->form_validation->set_policy('enable_dhcp', 'dhcp/Subnets_Class', 'validate_dhcp_state');
            else if (clearos_library_installed('dhcp/Dnsmasq'))
                $this->form_validation->set_policy('enable_dhcp', 'dhcp/Dnsmasq', 'validate_dhcp_state');
        } else if ($bootproto == IfaceAPI::BOOTPROTO_DHCP)  {
            $this->form_validation->set_policy('hostname', 'network/Iface', 'validate_hostname');
            $this->form_validation->set_policy('dhcp_dns', 'network/Iface', 'validate_peerdns');
        } else if ($bootproto == IfaceAPI::BOOTPROTO_PPPOE)  {
            $this->form_validation->set_policy('username', 'network/Iface', 'validate_username', TRUE);
            $this->form_validation->set_policy('password', 'network/Iface', 'validate_password', TRUE);
            $this->form_validation->set_policy('mtu', 'network/Iface', 'validate_mtu');
            $this->form_validation->set_policy('pppoe_dns', 'network/Iface', 'validate_peerdns');
        } else if ($bootproto == IfaceAPI::BOOTPROTO_NONE) {
            // No validation required
        }

        if ($role == Role::ROLE_EXTERNAL) {
            $this->form_validation->set_policy('max_upstream', 'network/Iface', 'validate_max_upstream');
            $this->form_validation->set_policy('max_downstream', 'network/Iface', 'validate_max_downstream');

            $this->form_validation->set_policy('proxy_server', 'network/Proxy', 'validate_server');
            $this->form_validation->set_policy('proxy_port', 'network/Proxy', 'validate_port');
            $this->form_validation->set_policy('proxy_username', 'network/Proxy', 'validate_username');
            $this->form_validation->set_policy('proxy_password', 'network/Proxy', 'validate_password');
        }

        $form_ok = $this->form_validation->run();

        // Get list of unconfigured interfaces
        //------------------------------------

        try {
            $unconfigured = $this->iface_manager->get_unconfigured_interfaces();
            $data['type'] = $this->iface->get_type();
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Handle form submit
        //-------------------
    
        if ($this->input->post('submit') && ($form_ok === TRUE)) {
            try {
                if ($bootproto == IfaceAPI::BOOTPROTO_BRIDGED){
                    $this->iface->disable($interface);
                    $this->role->remove_interface_role($interface);
                    $this->iface->save_bridge_minion_config($this->input->post('bridge_master'));
                    $this->iface->disable(FALSE);
                    $this->iface->enable(FALSE);
                    $this->page->set_status_updated();
                    redirect('/network/iface');
                } else if ($bootproto == IfaceAPI::BOOTPROTO_STATIC) {
                    $this->iface->save_static_config(
                        $data['type'],
                        $this->input->post('ipaddr'),
                        $this->input->post('netmask'),
                        $this->input->post('gateway'),
                        $options
                    );
                    $this->_update_routing($interface, $role);
                    $this->iface->disable(FALSE);
                    $this->iface->enable(FALSE);

                    // TODO: remove deprecated dhcp/Dnsmasq in ClearOS 8
                    if (clearos_library_installed('dhcp/Subnets_Class') && ($this->input->post('enable_dhcp')))
                        $this->subnets_class->add_default($interface);
                    else if (clearos_library_installed('dhcp/Dnsmasq') && ($this->input->post('enable_dhcp')))
                        $this->dnsmasq->add_subnet_default($interface);
                } else if ($bootproto == IfaceAPI::BOOTPROTO_DHCP) {
                    $this->iface->save_dhcp_config(
                        $data['type'],
                        $this->input->post('hostname'),
                        (bool) $this->input->post('dhcp_dns'),
                        $options
                    );

                    $this->_update_routing($interface, $role);
                    $this->iface->disable(FALSE);
                    $this->iface->enable(TRUE);
                } else if ($bootproto == IfaceAPI::BOOTPROTO_PPPOE) {
                    $interface = $this->iface->save_pppoe_config(
                        $interface,
                        $this->input->post('username'),
                        $this->input->post('password'),
                        $this->input->post('mtu'),
                        (bool) $this->input->post('pppoe_dns')
                    );

                    $this->_update_routing($interface, $role);
                    $this->iface->disable(FALSE);
                    $this->iface->enable(TRUE);
                } else if ($bootproto == IfaceAPI::BOOTPROTO_NONE) {
                    $this->iface->save_no_ip_config($data['type']);

                    $this->_update_routing($interface, $role);
                    $this->iface->disable(FALSE);
                    $this->iface->enable(FALSE);
                }

                // ISP bandwidth and upstream proxy settings
                //------------------------------------------

                if ($role == Role::ROLE_EXTERNAL) {
                    $this->iface->set_max_upstream($this->input->post('max_upstream'));
                    $this->iface->set_max_downstream($this->input->post('max_downstream'));

                    $this->proxy->set_server($this->input->post('proxy_server'));
                    $this->proxy->set_port($this->input->post('proxy_port'));
                    $this->proxy->set_username($this->input->post('proxy_username'));
                    $this->proxy->set_password($this->input->post('proxy_password'));
                    $this->proxy->write_profile();
                }

                // Return to summary page with status message
                //-------------------------------------------

                $this->page->set_status_updated();
                redirect('/network/iface');
            } catch (Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }

        // Load the view data
        //-------------------

        try {
            $options['filter_pppoe'] = ($data['type'] == IfaceAPI::TYPE_BRIDGED) ? TRUE : FALSE;
            
            $data['roles'] = $this->iface->get_supported_roles();
            $data['bootprotos'] = $this->iface->get_supported_bootprotos($options);
            $data['iface_info'] = $this->iface->get_info();

            $iface_count = $this->iface_manager->get_interface_count();

            // Default to enable on unconfigured interfaces
            if (clearos_app_installed('dhcp') && ($data['iface_info']['configured'] === FALSE)) {
                $data['show_dhcp'] = TRUE;
                $data['enable_dhcp'] = TRUE;
            } else if (clearos_app_installed('dhcp')) {
                // TODO: remove deprecated dhcp/Dnsmasq in ClearOS 8
                if (clearos_library_installed('dhcp/Subnets_Class')) {
                    $this->load->library('dhcp/Subnets_Class');
                    $subnets = $this->subnets_class->listing();
                } else if (clearos_library_installed('dhcp/Dnsmasq')) {
                    $this->load->library('dhcp/Dnsmasq');
                    $subnets = $this->dnsmasq->get_subnets();
                }

                if (array_key_exists($interface, $subnets)) {
                    $dhcp_info = $subnets[$interface];
                    // Hide DHCP status on networkin interface info when already configured
                    // to try and prevent users from accidently deleting their DHCP setup
                    if ($dhcp_info['isconfigured'])
                        $data['show_dhcp'] = FALSE;
                    else
                        $data['show_dhcp'] = TRUE;
                } else {
                    $data['show_dhcp'] = TRUE;
                    $data['enable_dhcp'] = TRUE;
                }
            } else {
                $data['show_dhcp'] = FALSE;
            }

            $data['proxy_server'] = $this->proxy->get_server();
            $data['proxy_port'] = $this->proxy->get_port(FALSE);
            $data['proxy_username'] = $this->proxy->get_username();
            $data['proxy_password'] = $this->proxy->get_password();
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Set defaults
        if (!$data['iface_info']['configured'])
            $data['iface_info']['role'] = ($iface_count === 1) ? Role::ROLE_EXTERNAL : Role::ROLE_LAN;

        if (empty($data['iface_info']['ifcfg']['bootproto']))
            $data['iface_info']['ifcfg']['bootproto'] = \clearos\apps\network\Iface::BOOTPROTO_STATIC;

        if (empty($data['iface_info']['ifcfg']['netmask']))
            $data['iface_info']['ifcfg']['netmask'] = '255.255.255.0';

        $data['form_type'] = $form_type;
        $data['interface'] = $interface;

        // Maximum ISP settings
        $data['max_downstream'] = $this->iface->get_max_downstream();
        $data['max_upstream'] = $this->iface->get_max_upstream();

        // Load the views
        //---------------

        $page_options['type'] = (clearos_console()) ? MY_Page::TYPE_CONSOLE : NULL;

        $this->page->view_form('network/iface', $data, lang('network_interface'), $page_options);
    }

    /**
     * Updates routing information
     *
     */

    function _update_routing($interface, $role)
    {
        // Load libraries
        //---------------

        $this->load->library('network/Routes');
        $this->load->library('network/Role');

        try {
            // Set routing
            //------------

            $current_route = $this->routes->get_gateway_device();

            if ($role === Role::ROLE_EXTERNAL) {
                $this->routes->set_gateway_device($interface);
            } else if ($interface == $current_route) {
                $this->routes->delete_gateway_device();
                // TODO: should try to restore route if possible
            }

            // Set interface role
            //-------------------

            $this->role->set_interface_role($interface, $role);
        } catch (\Exception $e) {
            $this->page->view_exception($e);
            return;
        }
    }

    /**
     * Common add/edit/view form handler for virtual interfaces.
     *
     * @param string $form_type form type
     * @param string $interface interface
     *
     * @return view
     */

    function _virtual_item($form_type, $interface)
    {
        if ($form_type === 'add')
            $interface = $this->input->post('iface');

        // Load libraries
        //---------------

        $this->lang->load('network');
        $this->load->library('network/Iface', $interface);
        $this->load->library('network/Iface_Manager');

        // Set validation rules
        //---------------------

        $this->form_validation->set_policy('ipaddr', 'network/Iface', 'validate_ip', TRUE);
        $this->form_validation->set_policy('netmask', 'network/Iface', 'validate_netmask', TRUE);

        $form_ok = $this->form_validation->run();

        // Handle form submit
        //-------------------

        if ($this->input->post('submit') && ($form_ok === TRUE)) {

            try {
                $this->iface->save_virtual_config($this->input->post('ipaddr'), $this->input->post('netmask'));
                $this->iface->enable();

                $this->page->set_status_updated();
                redirect('/network/iface');
            } catch (Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }

        // Load the view data
        //-------------------

        try {
            $iface_options['filter_vlan'] = TRUE;
            $iface_options['filter_bridge'] = FALSE;
            $iface_options['filter_ppp'] = TRUE;

            $data['form_type'] = $form_type;
            $data['iface'] = $interface;
            $data['ifaces'] = $this->iface_manager->get_interfaces($iface_options);

            if ($form_type !== 'add')
                $data['iface_info'] = $this->iface->get_info();
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load the views
        //---------------

        $page_options['type'] = (clearos_console()) ? MY_Page::TYPE_CONSOLE : NULL;

        $this->page->view_form('network/virtual', $data, lang('network_add_virtual'), $page_options);
    }

    /**
     * Common add/edit/view form handler for VLAN interfaces.
     *
     * @param string $form_type form type
     * @param string $interface interface
     *
     * @return view
     */

    function _vlan_item($form_type, $interface)
    {
        // Load libraries
        //---------------

        $this->lang->load('network');
        $this->load->library('network/Iface', $interface);
        $this->load->library('network/Iface_Manager');
        $this->load->library('network/Role');

        // TODO: remove deprecated dhcp/Dnsmasq in ClearOS 8
        if (clearos_library_installed('dhcp/Subnets_Class'))
            $this->load->library('dhcp/Subnets_Class');
        else if (clearos_library_installed('dhcp/Dnsmasq'))
            $this->load->library('dhcp/Dnsmasq');

        // Set validation rules
        //---------------------

        $bootproto = $this->input->post('bootproto');
        $role = $this->input->post('role');

        $this->form_validation->set_policy('role', 'network/Role', 'validate_role', TRUE);
        $this->form_validation->set_policy('bootproto', 'network/Iface', 'validate_boot_protocol', TRUE);
        $this->form_validation->set_policy('vlan_id', 'network/Iface', 'validate_vlan_id', TRUE);

        if ($bootproto == IfaceAPI::BOOTPROTO_STATIC) {
            $this->form_validation->set_policy('ipaddr', 'network/Iface', 'validate_ip', TRUE);
            $this->form_validation->set_policy('netmask', 'network/Iface', 'validate_netmask', TRUE);
            // TODO: remove deprecated dhcp/Dnsmasq in ClearOS 8
            if ($role == Role::ROLE_EXTERNAL)
                $this->form_validation->set_policy('gateway', 'network/Iface', 'validate_gateway', TRUE);
            else if (clearos_library_installed('dhcp/Subnets_Class'))
                $this->form_validation->set_policy('enable_dhcp', 'dhcp/Subnets_Class', 'validate_dhcp_state');
            else if (clearos_library_installed('dhcp/Dnsmasq'))
                $this->form_validation->set_policy('enable_dhcp', 'dhcp/Dnsmasq', 'validate_dhcp_state');
        } else if ($bootproto == IfaceAPI::BOOTPROTO_DHCP)  {
            $this->form_validation->set_policy('hostname', 'network/Iface', 'validate_hostname');
            $this->form_validation->set_policy('dhcp_dns', 'network/Iface', 'validate_peerdns');
        } else if ($bootproto == IfaceAPI::BOOTPROTO_PPPOE)  {
            $this->form_validation->set_policy('username', 'network/Iface', 'validate_username', TRUE);
            $this->form_validation->set_policy('password', 'network/Iface', 'validate_password', TRUE);
            $this->form_validation->set_policy('mtu', 'network/Iface', 'validate_mtu');
            $this->form_validation->set_policy('pppoe_dns', 'network/Iface', 'validate_peerdns');
        } 

        $form_ok = $this->form_validation->run();

        // Handle form submit
        //-------------------

        if ($this->input->post('submit') && ($form_ok === TRUE)) {
            if ($form_type === 'add')
                $interface = $this->input->post('iface') . '.' . $this->input->post('vlan_id');

            try {
                if ($bootproto == IfaceAPI::BOOTPROTO_STATIC) {
                    $this->iface->save_vlan_static_config(
                        $this->input->post('vlan_id'),
                        $this->input->post('ipaddr'),
                        $this->input->post('netmask'),
                        $this->input->post('gateway')
                    );

                    $this->_update_routing($interface, $role);
                    $this->iface->enable(FALSE);

                    // TODO: remove deprecated dhcp/Dnsmasq in ClearOS 8
                    if (clearos_library_installed('dhcp/Subnets_Class') && ($this->input->post('enable_dhcp')))
                        $this->subnets_class->add_default($interface);
                    else if (clearos_library_installed('dhcp/Dnsmasq') && ($this->input->post('enable_dhcp')))
                        $this->dnsmasq->add_subnet_default($interface);
                } else if ($bootproto == IfaceAPI::BOOTPROTO_DHCP) {
                    $this->iface->save_vlan_dhcp_config(
                        $this->input->post('vlan_id'),
                        $this->input->post('hostname'),
                        (bool) $this->input->post('dhcp_dns')
                    );

                    $this->_update_routing($interface, $role);
                    $this->iface->enable(TRUE);
                }  else if ($bootproto == IfaceAPI::BOOTPROTO_PPPOE) {
                    
                    $interface = $this->iface->save_pppoe_config(
                        $interface,
                        $this->input->post('username'),
                        $this->input->post('password'),
                        $this->input->post('mtu'),
                        (bool) $this->input->post('pppoe_dns')
                    );

                    $this->_update_routing($interface, $role);
                    $this->iface->disable(FALSE);
                    $this->iface->enable(TRUE);
                } 


                
                // Return to summary page with status message
                //-------------------------------------------

                $this->page->set_status_updated();
                redirect('/network/iface');
            } catch (Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }

        // Load the view data
        //-------------------

        try {
            //$options['filter_pppoe'] = TRUE;
            //$options['filter_pppoe'] = ($data['type'] == IfaceAPI::TYPE_BRIDGED) ? TRUE : FALSE;
            $options['filter_pppoe'] = FALSE;
            $iface_options['filter_vlan'] = TRUE;
            $iface_options['filter_bridge'] = FALSE;
            $iface_options['filter_ppp'] = TRUE;

            $data['form_type'] = $form_type;
            $data['iface'] = $interface;
            $data['ifaces'] = $this->iface_manager->get_interfaces($iface_options);
            $data['roles'] = $this->iface->get_supported_roles();
            $data['bootprotos'] = $this->iface->get_supported_bootprotos($options);
            $data['max_downstream'] = $this->iface->get_max_downstream();
            $data['max_upstream'] = $this->iface->get_max_upstream();

            if ($form_type !== 'add')
                $data['iface_info'] = $this->iface->get_info();

            // Default to enable on unconfigured interfaces
            if (clearos_app_installed('dhcp') && ($form_type === 'add')) {
                $data['show_dhcp'] = TRUE;
                $data['enable_dhcp'] = TRUE;
            } else {
                $data['show_dhcp'] = FALSE;
            }
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Set defaults
        if (!$data['iface_info']['configured'])
            $data['iface_info']['role'] = Role::ROLE_LAN;

        if (empty($data['iface_info']['ifcfg']['bootproto']))
            $data['iface_info']['ifcfg']['bootproto'] = \clearos\apps\network\Iface::BOOTPROTO_STATIC;

        if (empty($data['iface_info']['ifcfg']['netmask']))
            $data['iface_info']['ifcfg']['netmask'] = '255.255.255.0';

        // Load the views
        //---------------

        $page_options['type'] = (clearos_console()) ? MY_Page::TYPE_CONSOLE : NULL;

        $this->page->view_form('network/vlan', $data, lang('network_add_vlan'), $page_options);
    }
}
